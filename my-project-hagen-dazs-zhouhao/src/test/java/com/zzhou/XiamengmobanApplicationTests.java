package com.zzhou;

import com.zzhou.auth.entity.AdminInfo;
import com.zzhou.auth.mapper.AdminInfoMapper;
import com.zzhou.auth.service.AdminInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class XiamengmobanApplicationTests {

    @Autowired
    private AdminInfoService adminInfoService;

    @Autowired
    private AdminInfoMapper adminInfoMapper;
    @Test
    void contextLoads() {
        List<AdminInfo> list = adminInfoService.list();
        System.out.println(list);
    }

    @Test
    void less(){

        int i = adminInfoMapper.deleteById(9L);
        System.out.println(i);
    }

}
