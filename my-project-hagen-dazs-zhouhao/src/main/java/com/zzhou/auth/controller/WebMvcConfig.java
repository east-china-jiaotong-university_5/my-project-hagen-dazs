package com.zzhou.auth.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: xiamengmoban
 * @description:
 * @author: 周浩
 * @create: 2023-07-25 09:11
 **/
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("index.html").setViewName("index");
        registry.addViewController("about.html").setViewName("about");
        registry.addViewController("add.html").setViewName("add");
        registry.addViewController("Administrator.html").setViewName("Administrator");
        registry.addViewController("blog.html").setViewName("blog");
        registry.addViewController("brand.html").setViewName("brand");
        registry.addViewController("brownie-ice-cream.html").setViewName("brownie-ice-cream");
        registry.addViewController("category.html").setViewName("category");
        registry.addViewController("checkout.html").setViewName("checkout");
        registry.addViewController("contact.html").setViewName("contact");
        registry.addViewController("delete.html").setViewName("delete");
        registry.addViewController("delivery-information.html").setViewName("delivery-information");
        registry.addViewController("forgot.html").setViewName("forgot");
        registry.addViewController("fresh-fruit-icecream.html").setViewName("fresh-fruit-icecream");
        registry.addViewController("fruit-candy.html").setViewName("fruit-candy");
        registry.addViewController("gift-certificate.html").setViewName("gift-certificate");
        registry.addViewController("ice-cream-cones.html").setViewName("ice-cream-cones");
        registry.addViewController("login.html").setViewName("login");
        registry.addViewController("privacy-policy.html").setViewName("privacy-policy");
        registry.addViewController("product.html").setViewName("product");
        registry.addViewController("product-comparison.html").setViewName("product-comparison");
        registry.addViewController("product-returns.html").setViewName("product-returns");
        registry.addViewController("register.html").setViewName("register");
        registry.addViewController("shop.html").setViewName("shop");
        registry.addViewController("shopping-cart.html").setViewName("shopping-cart");
        registry.addViewController("site-map.html").setViewName("site-map");
        registry.addViewController("sp-oreo-shake.html").setViewName("sp-oreo-shake");
        registry.addViewController("terms-conditions.html").setViewName("terms-conditions");
        registry.addViewController("zhouhao.html").setViewName("zhouhao");
        registry.addViewController("success.html").setViewName("success");
    }
}
