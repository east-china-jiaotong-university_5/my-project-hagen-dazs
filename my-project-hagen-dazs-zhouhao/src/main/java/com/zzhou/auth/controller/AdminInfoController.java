package com.zzhou.auth.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author zhouhao
 * @since 2023-07-22
 */
@RestController
@RequestMapping("/auth/admin-info")
public class AdminInfoController {

}

