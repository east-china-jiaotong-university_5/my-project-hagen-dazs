package com.zzhou.auth.controller;

import com.zzhou.auth.entity.AdminInfo;
import com.zzhou.auth.mapper.AdminInfoMapper;
import com.zzhou.auth.service.AdminInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: xiamengmoban
 * @description:
 * @author: 周浩
 * @create: 2023-07-22 15:26
 **/
@Controller
public class ThController {

    @Autowired
    private AdminInfoService adminInfoService;

    @Autowired
    private AdminInfoMapper adminInfoMapper;

    @RequestMapping("/Administrator.html")
    public String show(Model model){
        String s = "周浩";
        model.addAttribute("s",s);
        List<AdminInfo> list = adminInfoService.list();
        model.addAttribute("ad",list);
        ArrayList<Object> list1 = new ArrayList<>();
        list1.add("周浩");
        model.addAttribute("name",list1);
        return "Administrator";
    }

    @RequestMapping("/tadd")
    public String MytAdd(){
        return "add";
    }
    @PostMapping("/add")
    public String MyAdd(AdminInfo adminInfo){
        adminInfoService.save(adminInfo);
        return "delete";
    }

    @GetMapping("/delete")
    public String MyDelete(Long userid){
        System.out.println(userid);
        boolean b = adminInfoService.removeById(userid);
        System.out.println(b);
        return "delete";
    }

}
