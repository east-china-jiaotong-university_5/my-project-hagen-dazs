package com.zzhou.auth.service;

import com.zzhou.auth.entity.AdminInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author zhouhao
 * @since 2023-07-22
 */
@Service
public interface AdminInfoService extends IService<AdminInfo> {

}
