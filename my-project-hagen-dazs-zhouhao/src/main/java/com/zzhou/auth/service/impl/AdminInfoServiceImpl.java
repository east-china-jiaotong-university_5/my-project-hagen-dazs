package com.zzhou.auth.service.impl;

import com.zzhou.auth.entity.AdminInfo;
import com.zzhou.auth.mapper.AdminInfoMapper;
import com.zzhou.auth.service.AdminInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author zhouhao
 * @since 2023-07-22
 */
@Service
public class AdminInfoServiceImpl extends ServiceImpl<AdminInfoMapper, AdminInfo> implements AdminInfoService {

}
