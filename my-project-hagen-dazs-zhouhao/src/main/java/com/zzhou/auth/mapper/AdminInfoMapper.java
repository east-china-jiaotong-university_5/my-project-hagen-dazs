package com.zzhou.auth.mapper;

import com.zzhou.auth.entity.AdminInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author zhouhao
 * @since 2023-07-22
 */
@Mapper
public interface AdminInfoMapper extends BaseMapper<AdminInfo> {

}
