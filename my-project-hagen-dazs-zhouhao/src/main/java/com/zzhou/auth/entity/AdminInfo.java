package com.zzhou.auth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author zhouhao
 * @since 2023-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId("userId")
    private Long userid;

    /**
     * 用户名字
     */
    @TableField("userName")
    private String username;

    /**
     * 用户密码
     */
    @TableField("userPass")
    private String userpass;

    /**
     * 用户邮件
     */
    @TableField("userMail")
    private String usermail;

    /**
     * 电话
     */
    @TableField("userPhone")
    private String userphone;

    /**
     * 状态：1启用0禁用
     */
    @TableField("userUsed")
    private String userused;


}
